const electron = require('electron')
// Module to control application life.
const app = electron.app

app.allowRendererProcessReuse = false // to prevent deprecation warning

// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
// Module for global shortcuts
const globalShortcut = electron.globalShortcut
//
const windowStateKeeper = require('electron-window-state');
//
const {ipcMain, session} = require('electron')

const Menu = electron.Menu
//
const hasFlag = require('has-flag');
const argv = require('minimist')(process.argv.slice(2));
//
const Config = require('electron-config');
const config = new Config();

const is = require('electron-is');
const log = require('electron-log');

// setLogLevel( (is.dev) ? 'info' : 'error' )
setLogLevel( (is.dev) ? 'debug' : 'error' )

const chalk = require('chalk');
const moment = require('moment');
const internalIp = require('internal-ip');

// cap

// var ZwiftPacketMonitor;

if (argv.nocap) {
  global.errors = ['Running with option --nocap']
} else if (config.has('enabled.cap') ? config.get('enabled.cap') : false) {
  // Listen if enabled.cap except when parameter --nocap is used
  // console.log('Will try to listen for Zwift network traffic...')
  log.info('Will try to listen for Zwift network traffic...')
  try {
      var ZwiftPacketMonitor = require('@zwfthcks/zwift-packet-monitor');
      var monitor
  } catch(e) {
    // just ignore silently -- assume it is because of lack of Cap-drivers
    // console.log(e)
    log.warn(e.message)
  }
} else {
  log.info('Listening for Zwift network traffic is disabled')
}


// globals 

global.color = { 'use-default-color': true }
global.background = '0.0'
global.track = { 'me': true, 'followees': true }
global.roads = { 'hide-running-only-roads': false }
// global.log = { 'file': log.transports.file.findLogPath() }
global.log = { 'file': log.transports.file.getFile().path }
global.weight = 0
global.zwiftgps = { clearcache: true, 'auto-login': false, 'reset-zoom': true  }
// global.resetZoom = true;
global.myzwiftcom = { 'auto-login': false, 'auto-close': false }
if (ZwiftPacketMonitor) {
  global.enabled = { 'chat': true, 'stat': true}
} else {
  // global.enabled = { 'chat': false, 'stat': false}
  // Now enabled to show whatever data is available in log.txt
  global.enabled = { 'chat': true, 'stat': true }
}
global.rideOns = 0;

// Initialise state variables
let state = {ignoreMouseEvents: true, asOverlay: {zwiftgps: true, map: true, chat: true} }

// Default values for options
let miscOptions = { asOverlay: true }

let windowOptions = {
  // all: { asOverlay: true },
  main: { 
    defaults: { width: 380, height: 750 },
    windowName: 'main',
    fileName: 'config/config.html',
    browserOptionsWindow: { webPreferences: {nodeIntegration: true, contextIsolation: false, enableRemoteModule: true} },
    allowOverlay: false
  },
  chat: {
    defaults: {width: 450, height: 300}, 
    windowName: 'chat',
    fileName: 'chat/chat.html',
    browserOptionsOverlay: {alwaysOnTop: true, transparent: true, frame: false, webPreferences: {nodeIntegration: true, contextIsolation: false, enableRemoteModule: true} }, // , backgroundColor: undefined},
    browserOptionsWindow: {alwaysOnTop: true, transparent: false, frame: true, backgroundColor: '#802c2c2c', webPreferences: {nodeIntegration: true, contextIsolation: false, enableRemoteModule: true} },
    allowOverlay: true,
    allowToggleWindowState: true,
    allowIgnoreMouse: true
  },
  stat: {
    defaults: {width: 1200, height: 180}, 
    windowName: 'stat',
    fileName: 'stat/stat.html',
    browserOptionsOverlay: {alwaysOnTop: true, transparent: true, frame: false, webPreferences: {nodeIntegration: true, contextIsolation: false, enableRemoteModule: true} }, // , backgroundColor: undefined},
    browserOptionsWindow: {alwaysOnTop: true, transparent: false, frame: false, webPreferences: {nodeIntegration: true, contextIsolation: false, enableRemoteModule: true} }, // , backgroundColor: '#802c2c2c'},
    allowOverlay: true,
    onlyOverlay: true,
    allowToggleWindowState: true,
    allowIgnoreMouse: false
  },
  map: {
    defaults: {width: 600, height: 600},
    windowName: 'map',
    fileName: 'map/map.html',
    browserOptionsOverlay: {alwaysOnTop: true, transparent: true, frame: false, webPreferences: {nodeIntegration: true, contextIsolation: false, enableRemoteModule: true} }, // , backgroundColor: undefined},
    browserOptionsWindow: {alwaysOnTop: true, transparent: false, frame: true, backgroundColor: '#802c2c2c', webPreferences: {nodeIntegration: true, contextIsolation: false, enableRemoteModule: true} },
    allowOverlay: true,
    allowToggleWindowState: true,
    allowIgnoreMouse: true
  },
  login: {
    defaults: { width: 400, height: 350 },
    windowName: 'login',
    fileName: 'login/login.html',
    browserOptionsWindow: { alwaysOnTop: true, frame: false, webPreferences: {nodeIntegration: true, contextIsolation: false, enableRemoteModule: true}  }, 
    allowOverlay: false
  },
  savelogin: {
    defaults: { width: 400, height: 350 },
    windowName: 'savelogin',
    fileName: 'savelogin/savelogin.html',
    browserOptionsWindow: { alwaysOnTop: true, frame: false, webPreferences: {nodeIntegration: true, contextIsolation: false, enableRemoteModule: true}  }, 
    allowOverlay: false
  },
  ghosts: {
    defaults: { width: 300, height: 500 },
    windowName: 'ghosts',
    url: '/editghosts',
    browserOptionsWindow: { webPreferences: {nodeIntegration: true, contextIsolation: false, enableRemoteModule: true} }, 
    allowOverlay: false
  },
  myzwiftcom: {
    defaults: { width: 500, height: 600 },
    windowName: 'myzwiftcom',
    fileName: 'myzwiftcom/index.html',
    browserOptionsWindow: { 
      show: true, 
      webPreferences: {
        partition: 'myzwiftcom',
        preload: './preload.js',
        nodeIntegration: true, contextIsolation: false, enableRemoteModule: true,
        webviewTag: true
      }
    }, 
    allowOverlay: false
  },
  zwiftgps: {
    defaults: { width: 800, height: 800 },
    windowName: 'zwiftgps',
    fileName: 'zwiftgps/index.html',
    loadURLOptions: { userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36' },
    browserOptionsOverlay: {
      alwaysOnTop: true, 
      transparent: true, 
      frame: false, 
      resizable: true,  
      webPreferences: {
        partition: 'zwiftgps',
        preload: './preload.js',
        nodeIntegration: true, contextIsolation: false, enableRemoteModule: true,
        webviewTag: true
      }
    }, 
    browserOptionsWindow: {
      alwaysOnTop: true,
      transparent: false, 
      frame: true,  
      webPreferences: {
        partition: 'zwiftgps',
        preload: './preload.js', 
        nodeIntegration: true, contextIsolation: false, enableRemoteModule: true
      }
    }, 
    allowOverlay: true,
    allowToggleWindowState: true,
    allowIgnoreMouse: true
  }
}


// Configuration / options
if (process.env.ZWIFTMAP_WINDOW == 1 || hasFlag('window') || (config.has('mode.window') ? config.get('mode.window') : false)) {
	global.mode = { 'window': true }
  miscOptions.asOverlay = false
  // state.asOverlay = {map: false, chat: false, stat: false}
  state.asOverlay = {map: false, chat: false}
  state.ignoreMouseEvents = false

  // app.disableHardwareAcceleration can only be called before app is ready
  // Therefore only called if normal window mode is forced
  // Necessary if OBS Studio has to be able to capture the window:
  app.disableHardwareAcceleration()

}

if ((config.has('graphics.disable-hw-acceleration') ? config.get('graphics.disable-hw-acceleration') : false) && hasFlag('disablehwacc')) {
  // disable if option is explicitly set  
  app.disableHardwareAcceleration()
}


// menu template
let template = [
  { label: app.name, submenu: [
    { label: 'Toggle window state', click() { actionOnGlobalShortcutZ() } },
    { type: 'separator' },
    { role: 'quit' }
  ] }
]

const menu = Menu.buildFromTemplate(template)

const path = require('path')
const url = require('url')

// Keep global references of the window objects, if we don't, the windows will
// be closed automatically when the JavaScript object is garbage collected.
let windows = { chat: null, map: null, main: null, login: null, savelogin: null, ghosts: null, stat: null, myzwiftcom: null, zwiftgps: null }

// the window names are used later when handling the window states
const windowNameList = ['main','map','chat', 'login', 'savelogin', 'ghosts', 'stat', 'myzwiftcom', 'zwiftgps']

// collection of windowStateKeepers
let stateKeepers = []


/**
 * 
 */
function createMainWindow () {
  createWindow('main', false)
}

/**
 * 
 */
function createMapWindow () {
  createWindow('map', state.asOverlay.map)
}

/**
 * 
 */
function createChatWindow () {
  if (global.enabled.chat) createWindow('chat', state.asOverlay.chat);
}


/**
 * 
 */
function createStatWindow () {
  if (global.enabled.stat) createWindow('stat', state.asOverlay.stat);
}



// 
const ZwiftGPS = require('./zwiftgps-helper.js');
const zwiftGPS = new ZwiftGPS();


/**
 * 
 * @param {*} name 
 * @param {*} asOverlay 
 */
function createWindow (name, asOverlay) {
  // Load the previous state with fallback to defaults
  let windowState = windowStateKeeper({
    defaultWidth: windowOptions[name].defaults.width,
    defaultHeight: windowOptions[name].defaults.height,
    file: 'window-state-' + windowOptions[name].windowName + '.json'
  });
  stateKeepers.push(windowState)
  
  let browserOptions

  if (asOverlay) {
    browserOptions = windowOptions[name].browserOptionsOverlay
  } else {
    browserOptions = windowOptions[name].browserOptionsWindow
  }

  // console.log(JSON.stringify(windowOptions[name], null, 4))
  log.debug(JSON.stringify(windowOptions[name], null, 4))

  browserOptions.width = windowState.width
  browserOptions.height = windowState.height
    
  if (name !== 'main' && windowState.x === undefined) {
    defaultPosition = getDefaultPosition(name, browserOptions.width, browserOptions.height)
    browserOptions.x = defaultPosition.x
    browserOptions.y = defaultPosition.y
  } else {
    browserOptions.x = windowState.x,
    browserOptions.y = windowState.y
  }


  if (name == 'main' && windowState.x !== undefined) {
    // make sure that the main/config window is shown visibly on an active display
    // if the saved position is outside the currently visible area
    let mainDisplay = electron.screen.getDisplayNearestPoint({x: browserOptions.x, y: browserOptions.y})
    // console.log(mainDisplay)
    log.debug(mainDisplay)
    if ((mainDisplay.bounds.x <= browserOptions.x) && (mainDisplay.bounds.x + mainDisplay.bounds.width > browserOptions.x) && (mainDisplay.bounds.y <= browserOptions.y) && (mainDisplay.bounds.y + mainDisplay.bounds.height > browserOptions.y)) {
      // all is ok
    } else {
      // calculate a new position
      browserOptions.x = mainDisplay.bounds.x + (mainDisplay.bounds.width - browserOptions.width) / 2
      browserOptions.y = mainDisplay.bounds.y + (mainDisplay.bounds.height - browserOptions.height) / 2
    }
  }

	// Create the browser window.
  browserOptions.show = true // false  // Why does false cause a white background in overlay mode???
	windows[name] = new BrowserWindow(browserOptions)

  // windows[name].once('ready-to-show', () => {
  windows[name].once('ready-to-show', () => {
    log.debug(`Event ready-to-show for window ${name}`)
    windows[name].show()
  })

  if (name == 'zwiftgps') {
    windows[name].webContents.on('dom-ready', function () {
      log.debug(`Event dom-ready catched for window ${name}`)
      windows[name].webContents.send('use-mouse-to-drag', zwiftGPS.useMouseToDrag)
      windows[name].webContents.send('use-mouse-in-zwiftgps', zwiftGPS.useMouseInZwiftGPS)
    })
  }

  // windows[name].on('show', () => {
  windows[name].webContents.once('did-finish-load', () => {
        // JRN 2018-08-08
        if (name == 'map') {
          if ((world = locationData.getWorld()) > 0) {
            windows[name].webContents.send('set-world', world);
            // console.log(`Just asked map to change to world ${ world} in showWindow on did-finish-load`)
            log.debug(`Just asked map to change to world ${ world} in showWindow on did-finish-load`)
          } else {
            windows[name].webContents.send('set-world', '');
          }
        }
    

  })

  // Let us register listeners on the window, so we can update the state
  // automatically (the listeners will be removed when the window is closed)
  // and restore the maximized or full screen state
  windowState.manage(windows[name]);

  // console.log(__dirname, windowOptions[name].fileName)
  log.debug(__dirname, windowOptions[name].fileName)

  // and load the index.html of the app.
  const windowUrl = windowOptions[name].fileName
      ? url.format({
          pathname: path.join(__dirname, windowOptions[name].fileName),
          protocol: 'file:',
          slashes: true
        })
      : `http://localhost:${serverPort}${windowOptions[name].url}`;
  const windowLoadURLOptions = windowOptions[name].loadURLOptions ? windowOptions[name].loadURLOptions : {};
  // console.log(windowLoadURLOptions);
  log.debug(windowLoadURLOptions);
  windows[name].loadURL(windowUrl, windowLoadURLOptions)


  if (asOverlay) {
 		// Set application menu IF on mac AND asOverlay
 		if (process.platform == 'darwin') { Menu.setApplicationMenu(menu) }
 		// if (process.platform == 'win32') { Menu.setApplicationMenu(menu) }

	  // if (name != 'main' ) {
    if (name == 'zwiftgps') {
      windows[name].setIgnoreMouseEvents(zwiftGPS.ignoreMouseEvents());
    } else if (windowOptions[name].allowOverlay) {
      // The main window itself is not shown as an overlay, but...
      // for all windows except main: 
      windows[name].setIgnoreMouseEvents(state.ignoreMouseEvents && (windowOptions[name].allowIgnoreMouse))
    }
  }


  // Open the DevTools.
  if (hasFlag('devtools')) {
    windows[name].webContents.openDevTools()
  }

  // some debugging info
  // console.log(name, windows[name].webContents.session)
  log.debug(name, windows[name].webContents.session)

  // Emitted when the window is closed.
  windows[name].on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    windows[name] = null
    if (name == 'main' ) {
      // closing the main window will quit the application if on windows
      if (process.platform !== 'darwin') {
        app.quit()
      }
    } else if (name == 'ghosts' && windows.map) {
      windows.map.webContents.send('clear-activity');
    }
  })
}


//
function getDefaultPosition(name, width, height) {
  var position = {x: 0, y: 0}
  if (windows[name]) {
    if (name == 'chat') {
      position.x = windows['main'].getBounds().x + windows['main'].getBounds().width
      position.y = windows['main'].getBounds().y
    } else if (name == 'map') {
      position.x = windows['main'].getBounds().x - width
      position.y = windows['main'].getBounds().y
    } else if (name == 'zwiftgps') {
      position.x = windows['main'].getBounds().x - width
      position.y = windows['main'].getBounds().y
    } else if (name == 'stat') {
      position.x = windows['main'].getBounds().x 
      position.y = windows['main'].getBounds().y - windowOptions['stat'].defaults.height
    }
  }
  return position
}

// riderLogData and locationData objects
const RiderLogData = require('./rider-log-data')
const LocationData = require('./location-data')
const riderLogData = new RiderLogData()
const locationData = new LocationData(riderLogData)

let server = null,
    serverPort = null
/**
 * 
 */
function createLocalServer() {
  const Server = require('zwift-second-screen/server/server')
  server = new Server({
    subscribe: () => locationData.subscribe(),
    getRider: () => locationData
  },{
    site: { maintenanceMode: false}
  })
  server.start(serverPort)
}

/**
 *  Event handler for when we have updated position data
 */
function mapWinOnPosition(positions) {
  if (windows.stat) {
    if (positions[0] && positions[0].me) {
      windows.stat.webContents.send('new-stat', JSON.stringify(positions[0]))
    }
  }
  if (windows.map) {
    // log.info(positions);
    windows.map.webContents.send('set-position', JSON.stringify(positions));
  }
}
//
locationData.on('positions', mapWinOnPosition)

//
locationData.on('world', (world) => {
  if (windows['map']) {
    windows['map'].webContents.send('set-world', world);
    // console.log(`Just asked map to change to world ${ world}`)
    log.info(`Just asked map to change to world ${ world}`)

  }
})

// Chat data object (for playername resolution)
const ChatData = require('./chat-data')
const chatData = new ChatData()

// Event handler for when we have new chat messages
chatData.on('message', (msg) => {
  // console.log('chatWinOnMessage: ' + msg[0].timestamp + ' ' + JSON.stringify(msg))
  log.debug('chatWinOnMessage: ' + msg[0].timestamp + ' ' + JSON.stringify(msg))
  if (windows.chat) {
    windows.chat.webContents.send('chat', msg[0].timestamp, msg[0].playerID, msg[0].message, msg[0].firstName, msg[0].lastName);
  }
})



function startZwiftPacketMonitor(ip = undefined) {

  // just return if no ZwiftPacketMonitor  
  if (!ZwiftPacketMonitor) return;
  
  // else go ahead and set up the monitor

  log.debug('startZwiftPacketMonitor called with ip', ip)
  if (ip === undefined) {
    ip = internalIp.v4.sync();
    // console.log(chalk.blue('Listening on: ', ip ));
  }
  log.info('Listening on: ', ip );

  try {
    monitor = new ZwiftPacketMonitor(ip)
  } catch (e) {
    // just handle silently -- assume it is because of insufficient access rights to /dev/bpf* device
    // mark chat and stat as disabled
    // 2020-04-20 not anymore - just show what is possible from log.txt
    // global.enabled = { 'chat': false, 'stat': false}
    global.errors = [e.message]
    // console.log(e)
    log.warn(e.message)
  }
  
  if (monitor) {
    
    monitor.on('outgoingPlayerState', (playerState, serverWorldTime) => {
      // console.log(playerState)
      // this riders id is playerState.id
      // riderLogData.updatePosition(playerState.x, playerState.y)
      mapWinOnPosition([{
        ...playerState, 
        ...{
          firstName: '',
          lastName: '',
          me: true,
          weight: global.weight * 1000
        }
      }])
    })
      
    // monitor.on('incomingPlayerState', (playerState, serverWorldTime) => {
    //   // console.log(playerState)
    // })
      
    // The simplest way to monitor for chat messages:
    monitor.on('incomingPlayerSentMessage', (playerUpdate, payload, serverWorldTime) => {
        // console.log(playerUpdate)
        if (payload.to_rider_id != 0) {
            // log(chalk.green.bgYellow(JSON.stringify(payload)))
            // console.log(chalk.green.bgYellow(moment(1000*(1414016119 + serverWorldTime/1000)).format() + ' ' + payload.firstName + ' ' + payload.lastName + ': ' + payload.message))
            log.debug(chalk.green.bgYellow(moment(1000*(1414016119 + serverWorldTime/1000)).format() + ' ' + payload.firstName + ' ' + payload.lastName + ': ' + payload.message))
        } else {
            // log(chalk.green(JSON.stringify(payload)))
            // console.log(chalk.green(moment(1000*(1414016119 + serverWorldTime/1000)).format() + ' ' + payload.firstName + ' ' + payload.lastName + ': ' + payload.message))
            log.debug(chalk.green(moment(1000*(1414016119 + serverWorldTime/1000)).format() + ' ' + payload.firstName + ' ' + payload.lastName + ': ' + payload.message))
        }
        log.debug(JSON.stringify(payload))
        // windows['chat'].webContents.send('chat', moment(1000*(1414016119 + serverWorldTime/1000)).format('HH:mm:ss'), '', payload.message, payload.firstName, payload.lastName);
      chatData.newMessage(moment(1000 * (1414016119 + serverWorldTime / 1000)).format('HH:mm:ss'), '', payload.message, payload.firstName, payload.lastName);
    
    
    })
    
    // debug only
    // setInterval(() => {
       
    //   global.rideOns += 1;
    //   if (windows.stat) {
    //     windows.stat.webContents.send('new-stat', JSON.stringify({ rideOns: global.rideOns }))
    //   }
      
    // }, 5000);

    monitor.on('incomingPlayerGaveRideOn', (playerUpdate, payload, serverWorldTime) => {
        // console.log(playerUpdate)
        // log(chalk.yellow(JSON.stringify(payload)))
        // console.log(chalk.yellow(moment(1000*(1414016119 + serverWorldTime/1000)).format() + ' ' + 'Ride On from ' + payload.firstName + ' ' + payload.lastName))
        log.debug(chalk.yellow(moment(1000*(1414016119 + serverWorldTime/1000)).format() + ' ' + 'Ride On from ' + payload.firstName + ' ' + payload.lastName))
        if (windows['chat']) {
          windows['chat'].webContents.send('rideon', moment(1000*(1414016119 + serverWorldTime/1000)).format('HH:mm:ss'), payload.rider_id, payload.firstName, payload.lastName);
        }
        
      global.rideOns += 1;
      if (windows.stat) {
        windows.stat.webContents.send('new-stat', JSON.stringify({ rideOns: global.rideOns }))
      }
    })
    
    // The Zwift server sends states in batches. This event is emitted at the end of each incoming batch
    // monitor.on('endOfBatch', () => {
    //   // console.log('end of batch')
    // }))
    
  }

  // start the monitor
  try {
    monitor.start()
  } catch(e) {
    // just handle silently -- assume it is because of insufficient access rights to /dev/bpf* device
    // mark chat and stat as disabled
    // 2020-04-20 not anymore - just show what is possible from log.txt
    // global.enabled = { 'chat': false, 'stat': false }
    global.errors = [e.message]
    // console.log(e)
    log.warn(e.message)
  }

}


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {

  
  createMainWindow()

  
  let ret
  
  if (miscOptions.asOverlay) {
    // Register a 'Super+Z' shortcut listener.
	  ret = globalShortcut.register('Alt+Super+Z', actionOnGlobalShortcutZ)
    
	  if (!ret) {
      // console.log('registration failed: Alt+Super+Z')
	  	log.error('registration failed: Alt+Super+Z')
	  }
  }
  
  ret = globalShortcut.register('Alt+Super+S', actionOnGlobalShortcutS)
  
  if (!ret) {
    // console.log('registration failed: Alt+Super+S')
    log.error('registration failed: Alt+Super+S')
  }
  
  ret = globalShortcut.register('Alt+Super+X', actionFocusMainWindow)
  
  if (!ret) {
    // console.log('registration failed')
    log.error('registration failed: Alt+Super+X')
  }


  /* 
    alternative way of determining ip implemented 2019-01-10 using package default-network because Bitdefender kills process when internal-ip is used (actually problem with conhost.exe launched by dependency default-gateway).
    Not so easy to read but works for now at least
  */

  var route = require('default-network');
  var ip
  route.collect(function(error, data) {
    log.debug('route.collect data:', data);
    names = Object.keys(data);
    try {
      var ifs = require('os').networkInterfaces()[names[0]]  ;
      log.debug('ifs:', ifs)
      ip = ifs.filter(x => x.family === 'IPv4' && !x.internal)[0].address
    } catch (e) {
      // ip = undefined
    }
    startZwiftPacketMonitor(ip)
  });

})


/**
 * 
 */
function actionFocusMainWindow () {
	// console.log('Alt+Super+X is pressed')
	log.debug('Alt+Super+X is pressed')

  if (windows['main'].isVisible() && windows['main'].isFocused()) {
    windows['main'].minimize()
  } else if (windows['main'].isVisible()) {
    windows['main'].focus()
  } else {
    windows['main'].show()
  }

}

/**
 * 
 */
function actionOnGlobalShortcutS () {
  // console.log('Alt+Super+S is pressed')
  log.debug('Alt+Super+S is pressed')
  zwiftGPS.toggleUseMouseInZwiftGPS();
  if (zwiftGPS.useMouseInZwiftGPS) { zwiftGPS.useMouseToDrag = false}
  if (windows['zwiftgps']) {
    windows['zwiftgps'].setIgnoreMouseEvents(zwiftGPS.ignoreMouseEvents());
    windows['zwiftgps'].webContents.send('use-mouse-in-zwiftgps', zwiftGPS.useMouseInZwiftGPS)
    windows['zwiftgps'].webContents.send('use-mouse-to-drag', zwiftGPS.useMouseToDrag)
    // console.log('use-mouse-in-zwiftgps', zwiftGPS.useMouseInZwiftGPS, 'use-mouse-to-drag', zwiftGPS.useMouseToDrag)
    log.debug('use-mouse-in-zwiftgps', zwiftGPS.useMouseInZwiftGPS, 'use-mouse-to-drag', zwiftGPS.useMouseToDrag)
  }
}


/**
 * 
 */
function actionOnGlobalShortcutZ () {
	// console.log('Alt+Super+Z is pressed')
	log.debug('Alt+Super+Z is pressed')
	state.ignoreMouseEvents = !state.ignoreMouseEvents
  // console.log('changed state to \n' +  JSON.stringify(state, null, 4))
  log.debug('changed state to \n' +  JSON.stringify(state, null, 4))
  
  zwiftGPS.toggleUseMouseToDrag()
  if (zwiftGPS.useMouseToDrag) { zwiftGPS.useMouseInZwiftGPS = false }

  windowNameList.forEach( (name) => {
    // console.log(name)
    log.debug('Process window', name)
    if ((windowOptions[name]) && (windowOptions[name].allowToggleWindowState)) {
      if (windows[name]) {
        if (name == 'zwiftgps') {
          windows[name].setIgnoreMouseEvents(zwiftGPS.ignoreMouseEvents())
          windows[name].webContents.send('use-mouse-to-drag', zwiftGPS.useMouseToDrag)
          windows[name].webContents.send('use-mouse-in-zwiftgps', zwiftGPS.useMouseInZwiftGPS)
          // console.log('use-mouse-in-zwiftgps', zwiftGPS.useMouseInZwiftGPS, 'use-mouse-to-drag', zwiftGPS.useMouseToDrag)
          log.debug('use-mouse-in-zwiftgps', zwiftGPS.useMouseInZwiftGPS, 'use-mouse-to-drag', zwiftGPS.useMouseToDrag)
        } else {
          windows[name].setIgnoreMouseEvents(state.ignoreMouseEvents && (windowOptions[name].allowIgnoreMouse));
          windows[name].webContents.send('ignore-mouse', state.ignoreMouseEvents)
        }
      }
    }
  })
  
}


// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

//
app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (windows['main'] === null) {
    createMainWindow()
  }
})

//
app.on('will-quit', () => {
  // Unregister a shortcut.
  // globalShortcut.unregister('Alt+Super+Z')
  // Unregister all shortcuts.
  globalShortcut.unregisterAll()

  tail.unwatch()

  // try {
  //   if (ZwiftPacketMonitor && monitor && monitor.stop) monitor.stop();
  // } catch (e) {
  //   //
  // }

})


let locationDataUnsubscribe;
//
ipcMain.on('show-zwiftgps', (event, showZwiftGPS, asOverlay) => {
  showWindow('zwiftgps', showZwiftGPS, asOverlay)
})


ipcMain.on('show-map', (event, showMap, asOverlay) => {
  // subscribe to position data when map or stat is open
  if (showMap !== !!windows.map) {
    if (locationDataUnsubscribe && !windows.stat) {
      locationDataUnsubscribe()
      locationDataUnsubscribe = null
    } 

    if (showMap && !locationDataUnsubscribe) {
      locationDataUnsubscribe = locationData.subscribe()
    }
  }

  showWindow('map', showMap, asOverlay)
})


//
ipcMain.on('show-chat', (event, showChat, asOverlay) => {
  if (global.enabled.chat) showWindow('chat', showChat, asOverlay);
})

//
ipcMain.on('show-myzwiftcom', (event, showMyZwiftCom, asOverlay) => {
  // console.log('show-myzwiftcom')
  log.debug('show-myzwiftcom')
  // myzwiftcom DISABLED 2021-07-22
  // showWindow('myzwiftcom', showMyZwiftCom, asOverlay)
})

//
ipcMain.on('profile', (event, profile) => {
  // console.log(profile)
  log.debug('profile', profile)
  profileObj = JSON.parse(profile)
  windows.main.webContents.send('got-profile', profileObj)
})

//
ipcMain.on('show-stat', (event, showStat, asOverlay) => {
  // subscribe to position data when map or stat is open
  if (showStat !== !!windows.stat) {
    if (locationDataUnsubscribe && !windows.map) {
      locationDataUnsubscribe()
      locationDataUnsubscribe = null
    } 

    if (showStat && !locationDataUnsubscribe) {
      locationDataUnsubscribe = locationData.subscribe()
    }
  }

  if (global.enabled.stat) showWindow('stat', showStat, asOverlay);
})

/**
 * 
 * @param {*} name 
 * @param {*} show 
 * @param {*} asOverlay 
 */
function showWindow(name, show, asOverlay) {
   // force overlay if windowOptions says so
   asOverlay = asOverlay || (windowOptions[name].onlyOverlay == true)

   // prevent overlay if windowOptions says so
   asOverlay = asOverlay && (windowOptions[name].allowOverlay == true)

   
   if (show) {
    // if window already exists with same overlay status then do nothing
    if (windows[name] && (asOverlay == state.asOverlay[name])) {
      // no change
      return
    } 
    if (windows[name]) {
      // close existing window
      windows[name].close()
    }
    if (asOverlay != state.asOverlay[name]) {
      // change options
      if (windowOptions[name].allowOverlay) {
        // this window is in the set of windows forming the overlay.
        // the global asOverlay and ignoreMouseEvenst status is updated 
        // to reflect the global overlay status.
        miscOptions.asOverlay = asOverlay
        state.ignoreMouseEvents = asOverlay
      }
      state.asOverlay[name] = asOverlay
    }
    createWindow(name, asOverlay)
  } else {
    // remove window if it exists
    if (windows[name]) {
        windows[name].close()
    }
  }
}

//
ipcMain.on('gather-windows', () => {
  // console.log('gather-windows received')
  log.debug('gather-windows received')
  windowNameList.forEach((name, index, arr) => {
    if (windows[name] && name != 'main') {
      gatherWindow(name)
    }
  })
})

function gatherWindow(name) {
  // console.log('gatherWindow ' + name)
  log.debug('gatherWindow ' + name)
  let mainWindow = windows.main
  let mainDisplay = electron.screen.getDisplayNearestPoint({x: mainWindow.getBounds().x, y: mainWindow.getBounds().y})

  if (windows[name]) {
    currentWindow = windows[name]
    currentDisplay = electron.screen.getDisplayNearestPoint({x: currentWindow.getBounds().x, y: currentWindow.getBounds().y})

    let offsetX = currentWindow.getBounds().x - currentDisplay.bounds.x;
    let offsetY = currentWindow.getBounds().y - currentDisplay.bounds.y;

    currentWindow.setPosition(mainDisplay.bounds.x + offsetX, mainDisplay.bounds.y + offsetY)
  }
}

//
ipcMain.on('default-positions', () => {
  // console.log('default-positions received')
  log.debug('default-positions received')
  windowNameList.forEach((name, index, arr) => {
    if (windows[name] && name != 'main') {
      defaultPosition = getDefaultPosition(name, windows[name].getBounds().width, windows[name].getBounds().height)
      windows[name].setPosition(defaultPosition.x, defaultPosition.y)
    }
  })
})



//
ipcMain.on('run-server', (event, runServer, port) => {
  if (runServer) {
    if (server) {
      // if server already running then do nothing
      if (port === serverPort) return
      // stop and start on a different port
      server.stop()
    }
    // else start the server
    serverPort = port
    createLocalServer()
  } else {
    // stop server if it's running
    if (server) {
      server.stop()
      server = null
    }
  }
})

//
ipcMain.on('set-world', (event, world) => {
	  // if (windows['map']) {
      // windows['map'].webContents.send('set-world', world);
      locationData.setWorld(world)
    // }
})

//
ipcMain.on('change-color', (event, color) => {
  global.color = color
  // console.log(global.color)
  log.debug('change-color', global.color)
  if (windows['map']) {
    windows['map'].webContents.send('change-color')
  }
  event.returnValue = true
})

//
ipcMain.on('change-background', (event, opacity) => {
  global.background = '' + opacity + ''
  // console.log(global.background)
  log.debug('change-background', global.background)
  if (windows['map']) {
    windows['map'].webContents.send('change-background')
  }
  if (windows['zwiftgps']) {
    windows['zwiftgps'].webContents.send('change-background')
  }
  event.returnValue = true
})

//
ipcMain.on('change-roads', (event, roads) => {
  global.roads = roads
  log.debug('change-roads', global.roads)
  if (windows['map']) {
    windows['map'].webContents.send('change-roads')
  }
  if (windows['zwiftgps']) {
    windows['zwiftgps'].webContents.send('change-roads')
  }
  event.returnValue = true
})

//
ipcMain.on('change-trackers', (event, track) => {
  global.track = track
  if (windows['map']) {
    windows['map'].webContents.send('change-trackers')
  }
  event.returnValue = true
})

//
ipcMain.on('clear-window-state', (event) => {
  stateKeepers.forEach( (windowState, index, arr) => {
    windowState.unmanage()
    // windowState = null
    // arr.splice(index, 1)
  })
  windowNameList.forEach( (windowName) => {
      fs.unlink(app.getPath('userData')  + '/window-state-' + windowName + '.json', (err) => {
        if (err) return;
      })
  })
})

//
ipcMain.on('toggle-window-state', (event) => {
  // console.log('received toggle-window-state', miscOptions.asOverlay );
  log.debug('received toggle-window-state', miscOptions.asOverlay );
  if (miscOptions.asOverlay) {
    actionOnGlobalShortcutZ()
  }
})

//
ipcMain.on('toggle-zwiftgps-state', (event) => {
  // console.log('received toggle-zwiftgps-state');
  log.debug('received toggle-zwiftgps-state');
  actionOnGlobalShortcutS()
})

ipcMain.on('clear-window-cache', (event, w) => {
  if (windows[w]) {
    let s = windows[w].webContents.session
    s.clearCache(function(){;})
    s.clearStorageData({ storages: 'cookies' }, function(){windows[w].close();})
  } else {
    if (p = windowOptions[w].browserOptionsWindow.webPreferences.partition) {
      let s = session.fromPartition(p)
      s.clearCache(function(){;})
      s.clearStorageData({ storages: 'cookies' })
    }
  }

})

ipcMain.on('set-weight', (event, weight) => {
  global.weight = weight;
  // console.log('received weight ', weight)
  log.debug('received weight ', weight)
})

ipcMain.on('set-global', (event, name, value) => {
  global[name] = value;
  // console.log(`set global ${name}`, value)
  log.debug(`set global ${name}`, value)
})

//
ipcMain.on('begin-save-sign-in', (event, username, password) => {
  showWindow('savelogin', true, false)
})

//
ipcMain.on('cancel-save-sign-in', (event) => {
  showWindow('savelogin', false, false)
})

//
ipcMain.on('delete-sign-in', (event) => {
  delete global['username'] 
  delete global['password'] 
  showWindow('savelogin', false, false)
})

//
ipcMain.on('save-sign-in', (event, username, password) => {
  showWindow('savelogin', false, false)
  global['username'] = username
  global['password'] = password
})


let signInSender
//
ipcMain.on('begin-sign-in', (event, username, password) => {
  signInSender = event.sender
  showWindow('login', true, false)
})

//
ipcMain.on('begin-silent-sign-in', (event, username, password) => {
  signInSender = event.sender 
})
//
ipcMain.on('cancel-sign-in', (event) => {
  showWindow('login', false, false)
})

//
ipcMain.on('sign-in', (event, username, password) => {

  // 2018-06-12 Prevent sign-in to prevent use of Zwift API
  return;

  // const ZwiftAccount = require('zwift-mobile-api')
  // const Rider = require('zwift-second-screen/server/rider')

  // // Sign in user
  // const account = new ZwiftAccount(username, password);
  // if (is.dev && hasFlag("sim") && argv.rider) {
  //   // allow running as another rider (in development only)
  //   // Use command line arguments:
  //   // --sim --rider=<RIDERID>
  //   var rider = new Rider(account, argv.rider)
  // } else {
  //   var rider = new Rider(account)
  // } 
  
  // // use the account when chat messages are processed
  // chatData.setAccount(account);

  // // Get the profile (to check the credentials and get a name)
  // rider.getProfile()
  //   .then(profile => {
  //     // Hide the sign-in window
  //     showWindow('login', false, false)

  //     // Set source to API data
  //     locationData.setSource(rider);

  //     const name = `${profile.firstName} ${profile.lastName}`
  //     signInSender.send('signed-in', name)
  //     console.log(`sign-in as ${name}`)

  //     log.info(profile)
      
  //     // 
  //     // rider.getStatic()

  //   })
  //   .catch(error => {
  //     // Show the error on the sign-in window
  //     event.sender.send('error', error)
  //   });
})

//
ipcMain.on('sign-out', (event, username, password) => {
  // Set source back to log data
  locationData.setSource(riderLogData);
  // Set account used for chatData to null
  chatData.setAccount(null);

  // console.log('sign-out');
  log.debug('sign-out');
})

ipcMain.on('edit-ghosts', () => {
  showWindow('ghosts', true, false)
})

ipcMain.on('preview-activity', (event, riderId, activityId) => {
  if (windows.map) {
    const ghostsApi = locationData.getGhosts();
    if (ghostsApi && ghostsApi.getActivity && activityId) {
      ghostsApi.getActivity(riderId, activityId).then(activity => {
        windows.map.webContents.send('preview-activity', JSON.stringify(activity));
      });
    } else {
      windows.map.webContents.send('clear-activity');
    }
  }
})

//
ipcMain.on('set-log-level', (event, loglevel) => {
  loglevel = ((loglevel == 'false') ? false : loglevel);
  setLogLevel(loglevel);
})

//
function setLogLevel(loglevel) {
  if (is.dev) {
    log.transports.file.level = loglevel;
    log.transports.console.level = loglevel;
  } else {
    log.transports.file.level = loglevel;
    log.transports.console.level = false;
  }
  // console.log('Log level is ' + log.transports.file.level);
  log.info('Log level is ' + log.transports.file.level);
}




// Tail on log.txt is established

var Tail = require('always-tail');
var fs = require('fs');

var filename = app.getPath('documents') + "/Zwift/Logs/Log.txt";
if (hasFlag('test')) filename = './project_test/log.txt'
// console.log(filename)
log.debug('Zwift log file:', filename)
if (!fs.existsSync(filename)) {
  fs.writeFileSync(filename, "");
  log.debug('Did not find Zwift log file - writing an empty file')
}



var patterns = {
  world :    /\[([^\]]*)\]\s+Loading WAD file 'assets\/Worlds\/world(\d*)\/data.wad/g ,
  position : /\[([^\]]*)\]\s+FPS\s*\d{1,3}(?:|\.\d{1,3}),\s*(\S+),\s*(\S+),\s*(\S+)/ ,
  // chat :     /\[([^\]]*)\]\s+NETWORK:Heard area message from (\S*) \((.*)\)/ ,
  chat :     /\[([^\]]*)\]\s+Chat: (\D*)(\d+)\s\((\S+)\): (.*)/ ,
  // rideon :     /\[([^\]]*)\]\s+HUD_Notify: (\S*) says Ride On!/ 
  rideon :     /\[([^\]]*)\]\s+HUD_Notify: (.*) says Ride On!/ 
}

// could also use this for world:
// [8:50:32] GAME_LoadLevel()  worldID = 5

// [14:01:33] Chat: Boldrin 1262019 (World): Cansei 
// [14:01:45] Chat: 187475 (Paddock): Any
// [14:02:09] Chat: 8136 (GroupEvent): just a short 3% bump about midway - then rollers around volcano territory


// TO DO: Check that Zwift is running before bothering to read log and guess world

// Guess world by reading log initially 

{
  var  worldLoaded = 0;
  var zwiftLog = fs.readFileSync(filename, "utf8");

  while ((match = patterns.world.exec(zwiftLog)) !== null) {
    // match.forEach ( (mtch) => {
    //   console.log(mtch)
    // })
    worldLoaded = match[2];
  }
  // console.log(`Zwift seems to have loaded world: ${worldLoaded}`)
  log.info(`Zwift seems to have loaded world: ${worldLoaded}`)

  locationData.setWorld(worldLoaded)

}



// if (windows['map']) {
//   windows['map'].webContents.send('set-world', locationData.getWorld());
//   console.log(`Just asked map to change to world ${ locationData.getWorld()}`)
// }


//

var tail = new Tail(filename, '\n', { interval: 50 });
// interval is set explicitly to ensure sufficiently frequent polling

//
tail.on('line', function(data) {
  // console.log("got line:", data);
  // log.silly("got line:", data);
  
  // checking for pattern matches (most likely match is checked first (TODO: verify optimal order))
  
  if (!ZwiftPacketMonitor && (match  = patterns.position.exec(data))) {
    // position
	  // console.log("set-position", match[2], match[3], match[4])
	  // log.silly("set-position", match[2], match[3], match[4])
    riderLogData.updatePosition(match[2], match[4])

  } else if (!ZwiftPacketMonitor && (match = patterns.rideon.exec(data))) {
    // rideon
    // console.log("rideon", match[1], match[2])
    log.debug("rideon", match[1], match[2])
      // send message directly to chat window
      if (windows['chat']) {
        windows['chat'].webContents.send('rideon', match[1] , '', match[2], '');
    }

    global.rideOns += 1;
    if (windows.stat) {
      windows.stat.webContents.send('new-stat', JSON.stringify({ rideOns: global.rideOns }))
    }
    
  } else if (!ZwiftPacketMonitor && (match = patterns.chat.exec(data))) {
    // chat
    log.info("chat", match[1], match[2], match[3], match[4], match[5])
    if (chatData) {
      // chatData.newMessage(match[1], match[2], match[3])
      chatData.newMessage(match[1], match[3], match[5], '(' + match[4] + ')', match[2])
      // Prototype: newMessage(timestamp, playerID, message, firstName = '', lastName = '') {
  
    } else {
      // fall back to just sending message directly to chat window
      if (windows['chat']) {
        //  matches are: 1 time  2 lastname  3 userid  4 type/scope  5 message
        //  send parameters are: time user message firstname lastname
        // windows['chat'].webContents.send('chat', match[1], match[2], match[3], '', '');
        windows['chat'].webContents.send('chat', match[1], match[3], match[5], '(' + match[4] + ')', match[2]);
      }
    }

  } else if (match = patterns.world.exec(data)) {
	  // world
    if (windows['map']) {
      // windows['map'].webContents.send('set-world', match[2]);
      locationData.setWorld(match[2])
    }
  } 

});


//
tail.on('error', function(data) {
  // console.log("error:", data);
  log.error("tail error:", data);
});

tail.watch();
// to unwatch and close all file descriptors, tail.unwatch();
