﻿class ZwiftGPS {
  constructor() {
    //super()

    this.useMouseToDrag = false;
    this.useMouseInZwiftGPS = false;

  }

  ignoreMouseEvents() {
    return !this.useMouseToDrag && !this.useMouseInZwiftGPS
  }

  toggleUseMouseToDrag() {
    this.useMouseToDrag = !this.useMouseToDrag
  }

  toggleUseMouseInZwiftGPS() {
      this.useMouseInZwiftGPS = !this.useMouseInZwiftGPS
  }

}

module.exports = ZwiftGPS
