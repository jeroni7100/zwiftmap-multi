GLOBAL DebugPrefix := "AHK: ToggleZwiftFullScreen: " 
GLOBAL DEBUG := FALSE
;GLOBAL DEBUG := DEBUGPREFIXPREFIX  ; uncomment line to activate debugging

;
GLOBAL Version := 1
GLOBAL VersionNoDate := "$Date: 2017/02/09 13:28:21 $"
GLOBAL VersionNo := "Ver. " Version " rev. " SubStr(VersionNoDate, 8, 19)

/* Changes :

*/


; Platform:       Win
; Author:         Jesper Rosenlund Nielsen <jesper@rosenlundnielsen.net>
;
; Script Function:
; Toggle pseudo full screen mode for zwift
;


#SingleInstance Force
#NoTrayIcon

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.



; #include lib_common\debug.ahk 

#include lib_zwift\zwift_app_class.ahk

;--

IfWinNotExist, % ZwiftApp.window
{
	; stop immediately if ZwiftApp is not running
	ExitApp
}

OnExit, Exit

; Save the taskbar state
TaskbarStatePre := GetTaskbarState()

TogglePseudoFullscreen(ZwiftApp.window)

WinWaitClose, % ZwiftApp.window

Return

; End of main routine
; ==========================================================


Exit:
{
	; Taskbar
	if (1 = GetTaskbarState() AND 1 <> TaskbarStatePre) {
		ToggleTaskbar()
	}
	;
	ExitApp
	Return
}

; ==========================================================


; ==========================
; Functions

; ----

TogglePseudoFullscreen(wnd) {
	WinGet, Style, Style, % wnd
	if (Style & 0xC00000)  {
		s := GetTaskbarState()
		
		if (1 <> GetTaskbarState()) {
			ToggleTaskbar()
		}
		WinMaximize, % wnd
		WinSet, Style, -0xC00000,% wnd
		; WinSet, Style, -0x40000, ahk_class GLFW30
	} else {
		if (1 = GetTaskbarState() AND 1 <> TaskbarStatePre) {
			ToggleTaskbar()
		}
		WinSet, Style, +0xC00000,% wnd
	}
	
}

; ----

ToggleTaskbar() {
	Global APPBARDATA
	VarSetCapacity(APPBARDATA, A_PtrSize=4 ? 36:48)

	NumPut(DllCall("Shell32\SHAppBarMessage", "UInt", 4 ; ABM_GETSTATE
                                           , "Ptr", &APPBARDATA
                                           , "Int")
	? 2:1, APPBARDATA, A_PtrSize=4 ? 32:40) ; 2 - ABS_ALWAYSONTOP, 1 - ABS_AUTOHIDE
	, DllCall("Shell32\SHAppBarMessage", "UInt", 10 ; ABM_SETSTATE
                                    , "Ptr", &APPBARDATA)

}

; ----

GetTaskbarState() {
	Global APPBARDATA
	VarSetCapacity(APPBARDATA, A_PtrSize=4 ? 36:48)

	CurrentState := DllCall("Shell32\SHAppBarMessage", "UInt", 4 ; ABM_GETSTATE
									   , "Ptr", &APPBARDATA
									   , "Int")
	return CurrentState
}





; ----------

ButtonFullscreenZwift:
{
	TogglePseudoFullscreen(ZwiftApp.window)
	return
}
