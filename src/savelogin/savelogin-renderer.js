﻿// This file is required by the login.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const Config = require('electron-config')
const config = new Config()

const {ipcRenderer} = require('electron')

const remote = require('electron').remote

var inputUsername = document.querySelector('#username')
var inputPassword = document.querySelector('#password')

var buttonSave = document.querySelector('#button-save')
var buttonDelete = document.querySelector('#button-delete')
var buttonCancel = document.querySelector('#button-cancel')


// read config
readConfig()

buttonSave.addEventListener('click', (evt) => {
  evt.preventDefault()
  buttonSave.disabled = true

  saveConfig()

  ipcRenderer.send('save-sign-in', inputUsername.value, inputPassword.value)
})

buttonCancel.addEventListener('click', () => {
  ipcRenderer.send('cancel-save-sign-in')
})

buttonDelete.addEventListener('click', (evt) => {
    evt.preventDefault()
    config.set('login.save', false)
    if (config.has('login.username')) config.delete('login.username');
    if (config.has('login.password')) config.delete('login.password');
    ipcRenderer.send('delete-sign-in')
  })

function readConfig() {
  var username = (config.has('login.username') ? config.get('login.username') : '')
  inputUsername.value = username

  if (username)
    inputPassword.focus()
  else
    inputUsername.focus()

  var save = (config.has('login.save') ? config.get('login.save') : false)

  if (save) {
    var password = (config.has('login.password') ? config.get('login.password') : '')
    inputPassword.value = password
  }
  
}

function saveConfig() {
  config.set('login.username', inputUsername.value)
  config.set('login.save', true)
  config.set('login.password', inputPassword.value)
}