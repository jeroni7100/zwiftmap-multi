// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.


const {ipcRenderer, remote} = require('electron')
const argv = require('minimist')(require('electron').remote.process.argv.slice(2));

var myWebview = document.getElementById('mywebview');

let riderProfile

myWebview.addEventListener("did-start-loading", function (e) {
  if (argv.devtools) {
     myWebview.openDevTools();
  }
});

// myWebview.addEventListener("did-finish-load", function (e) {
//   console.log('did-finish-load', e);
// });

// myWebview.addEventListener('dom-ready', function (e) {
//   console.log('dom-ready', e);
// })

// myWebview.addEventListener('console-message', (e) => {
//   // console.log('Guest page logged a message:', e.message)
// })
// myWebview.addEventListener('will-navigate', (e) => {
//   // console.log('Guest page will-navigate:', e.url)
// })
// myWebview.addEventListener('did-navigate', (e) => {
//   // console.log('Guest page did-navigate:', e.url)
// })
// myWebview.addEventListener('did-navigate-in-page', (e) => {
//   // console.log('Guest page did-navigate-in-page:', e);
// })


// Process the data from the webview
// Handle ipc-messages send with ipcRenderer.sendToHost in preload.js
myWebview.addEventListener('ipc-message', function(event){
    // console.log(event);
    // console.info(event.channel);
    if (event.channel == 'profile') {
      onMessageProfile(event)
    }
});

function onMessageProfile(event) {
  riderProfile = event.args[0]
  riderProfileObj = JSON.parse(riderProfile)
  // showMessage(JSON.stringify(JSON.parse(riderProfile), null, 4));
  // showMessage(`Got the profile.<br /><br />Your user id is ${riderProfileObj['id']} and your weight is ${((riderProfileObj.weight) ? riderProfileObj.weight + ' grams' : ' not defined')}`);
  showMessage(`Got the profile.<br /><br />Your user id is ${riderProfileObj['id']}`);
}

function doLogin() {
  usr = argv.username || remote.getGlobal('username');
  pwd = argv.password || remote.getGlobal('password');
  showMessage(`Ready for login as<br />${usr}`)
  if ((usr) && (pwd)) {
    console.log('LOGIN', usr, pwd)
    myWebview.executeJavaScript('window.doStuff();');
    // set timeout to reload in 5 seconds ...
    var reloadTimeout = setTimeout(function(){ myWebview.reload(); }, 5000);
    // ... unless the callback is fired because executeJavaScript fired as intended
    myWebview.executeJavaScript(`window.doLogin('${usr}', '${pwd}'); `, false, () => {  clearTimeout(reloadTimeout); showMessage('') /* prevent reload */ }) 
  }
}


function showMessage(text) {
	body = document.getElementsByTagName('body')[0];
	msg = document.createElement('div');
	body.appendChild(msg);
	msg.outerHTML = `<div style="position: absolute; left: 10px; top: 10px; width: 400px; height: 200px; border-radius: 10px; padding: 10px 10px; background-color: #424242; opacity: 0.9; color: white; ">${text}</div>`;
}

const regex = {
  login: /https:\/\/secure.zwift.com\/auth\/realms\/zwift\/protocol\/openid-connect\/auth\?response_type=code&client_id=my-zwift/m  
};

var waitingForLoginReady = false

myWebview.addEventListener('did-navigate', (e) => {
  console.log('Guest page did-navigate:', e)
  console.log('Info:', e.url)
  if ((regex.login.exec(e.url)) !== null) {
    waitingForLoginReady = true;
  }
})

var wait = ms => new Promise((r, j)=>setTimeout(r, ms))


myWebview.addEventListener('dom-ready', (e) => {
  console.log('Guest page dom-ready:', e)
  // console.log('Info:', e.url)
  if (waitingForLoginReady) {
    waitingForLoginReady = false;
    // (async () => {  res = false; do { await wait(5000); res = doLogin(); /* if (res != 'clicked') { myWebview.reload() } */ }  while ( res != 'clicked' ); })()
    (async () => {   await wait(5000);  doLogin();   })()
   
  }
})