﻿// This file is required by the login.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const Config = require('electron-config')
const config = new Config()

const {ipcRenderer} = require('electron')

const remote = require('electron').remote

var inputUsername = document.querySelector('#username')
var inputPassword = document.querySelector('#password')
var errorMessage = document.querySelector('#error-message')

var buttonLogin = document.querySelector('#button-login')
var buttonCancel = document.querySelector('#button-cancel')
var checkboxSave = document.querySelector('#save-password')


// read config
readConfig()

buttonLogin.addEventListener('click', (evt) => {
  evt.preventDefault()
  buttonLogin.disabled = true

  saveConfig()

  ipcRenderer.send('sign-in', inputUsername.value, inputPassword.value)
})

buttonCancel.addEventListener('click', () => {
  ipcRenderer.send('cancel-sign-in')
})

checkboxSave.addEventListener('change', () => {
    save = checkboxSave.checked
    // do some stuff based on value
    config.set('login.save', save)
    if (!save) {
      if (config.has('login.password')) config.delete('login.password');
    }
})

ipcRenderer.on('error', (event, error) => {
  buttonLogin.disabled = false

  let message
  if (error.response && error.response.status) {
    message = error.response.status + ' ' + error.response.statusText
    if (error.response.data && error.response.data.error_description) {
      message += " - " + error.response.data.error_description
    }
  } else {
    message = "Couldn't log in"
  }
  errorMessage.innerText = message
})

function readConfig() {
  var username = (config.has('login.uername') ? config.get('login.uername') : '')
  inputUsername.value = username

  if (username)
    inputPassword.focus()
  else
    inputUsername.focus()

  var save = (config.has('login.save') ? config.get('login.save') : false)
  checkboxSave.checked = save

  if (save) {
    var password = (config.has('login.password') ? config.get('login.password') : '')
    inputPassword.value = password
  }
  
}

function saveConfig() {
  config.set('login.uername', inputUsername.value)
  config.set('login.save', checkboxSave.checked)
  if (checkboxSave.checked) {
    config.set('login.password', inputPassword.value)
  } else {
    if (config.has('login.password')) config.delete('login.password');
  }
}