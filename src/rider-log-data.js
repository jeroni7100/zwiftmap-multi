﻿const EventEmitter = require('events')

class RiderLogData extends EventEmitter {
  constructor() {
    super()

    this.position = {
      x: 0,
      y: 0,
      speed: 0,
      power: 0,
      firstName: '',
      lastName: '',
      me: true
    }

    this.world = null

    this.state = {
      worldId: undefined,
      statusWorldId: undefined,
      filter: undefined
    };
  
  }

  updatePosition(x, y) {
    this.position.x = x
    this.position.y = y

    this.emit('positions', [this.position])
  }

  setWorld(world) {
    this.world = world
    this.emit('world', this.world)
  }

  getProfile() {
    return Promise.resolve({
      firstName: this.position.firstName,
      lastName: this.position.lastName,
      riding: true
    })
  }

  getPositions() {
    return Promise.resolve([this.position])
  }

  getWorld() {
    return this.world
  }

  
}

module.exports = RiderLogData
