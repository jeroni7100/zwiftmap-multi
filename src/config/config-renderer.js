// This file is required by the chat.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const Config = require('electron-config');
const config = new Config();

const electron = require('electron')
const {ipcRenderer} = require('electron')

const remote = require('electron').remote

const os = require('os')

var color = remote.getGlobal('color')
var enabled = remote.getGlobal('enabled')

const opn = require('opn')

const hpfy = require('../helpify/helpify.js');

const log = require('electron-log')

const is = require('electron-is');

/*

const app = require('electron').remote.app
const fs = require('fs')
const helpers = require('./helpers.js')

usercss = app.getPath('documents') + "/Zwift/zwiftmap/user.css"

fs.access(usercss, fs.constants.F_OK, (err) => {
  if (err == null) {
    //code when all ok
    helpers.loadcss(usercss)
  }
})

*/



var buttonSave = document.querySelector('#button-save');
var buttonDelete = document.querySelector('#button-delete');
var buttonResetWindows = document.querySelector('#button-reset-windows');
var buttonToggleWindows = document.querySelector('#button-toggle-windows');
var buttonToggleZwiftGPS = document.querySelector('#button-toggle-zwiftgps');
var inputShowMap = document.querySelector('#show-map')
var inputShowZwiftGPS = document.querySelector('#show-zwiftgps')

var inputShowStat = document.querySelector('#show-stat')
if (!enabled.stat) inputShowStat.disabled = true;
var inputShowChat = document.querySelector('#show-chat')
if (!enabled.chat) inputShowChat.disabled = true;

var inputEnableCap = document.querySelector('#enable-cap')

var inputHideRunningOnlyRoads = document.querySelector('#hide-running-only-roads')
var inputResetZoom = document.querySelector('#reset-zoom')
var inputShowJustMe = document.querySelector('#show-just-me')
var inputRunServer = document.querySelector('#run-server')
var inputServerPort = document.querySelector('#server-port')
var inputServerAddr = document.querySelector('#server-addr')

var inputUseDefaultColor = document.querySelector('#use-default-color')
var inputMapColor = document.querySelector('#map-color')
var inputRiderColor = document.querySelector('#rider-color')
var inputMeColor = document.querySelector('#me-color')
var inputMinimizeConfiguration = document.querySelector('#minimize-configuration')
var inputAsOverlay = document.querySelector('#as-overlay')
var inputWindowMode = document.querySelector('#window-mode')
var inputDisableHWAcceleration = document.querySelector('#disable-hw-acceleration')
var buttonWorld1 = document.querySelector('#world1')
var buttonWorld2 = document.querySelector('#world2')
var buttonWorld3 = document.querySelector('#world3')
var buttonWorld4 = document.querySelector('#world4')
var buttonWorld5 = document.querySelector('#world5')
var buttonWorld6 = document.querySelector('#world6')
var buttonWorld7 = document.querySelector('#world7')
var buttonWorld8 = document.querySelector('#world8')
var buttonWorld9 = document.querySelector('#world9')
var buttonWorld10 = document.querySelector('#world10')
var buttonWorld11 = document.querySelector('#world11')

var buttonNextDisplay = document.querySelector('#next-display')
var buttonPreviousDisplay = document.querySelector('#previous-display')
var buttonGatherWindows = document.querySelector('#gather-windows')
var buttonDefaultPositions = document.querySelector('#default-positions')

var buttonFullscreen = document.querySelector('#button-fullscreen')

buttonFullscreen.disabled = !is.windows()

// var captionSignIn = document.querySelector('#caption-sign-in')
// var buttonSignIn = document.querySelector('#button-sign-in')

var buttonSaveSignIn = document.querySelector('#button-save-sign-in')

// var buttonEditGhosts = document.querySelector('#button-edit-ghosts')

var buttonSaveWeightKg = document.querySelector('#save-weight-kg')
var buttonSaveWeightLbs = document.querySelector('#save-weight-lbs')
var inputWeight = document.querySelector('#weight')

var inputRiderId = document.querySelector('#rider-id')

var inputAutoRetrieveProfile = document.querySelector('#auto-retrieve-profile')
var inputAutoCloseMyZwiftCom = document.querySelector('#auto-close-myzwiftcom')
var inputAutoLoginZwiftGPS = document.querySelector('#auto-login-zwiftgps')


//
function isFirstRun() {
  // console.log('IN isFirstRun', config.size)
  return !(config.size > 0)
}  

function isFirstVersionRun() {
  // console.log('IN isFirstRun', config.size)
  return (!config.has('version') || (config.get('version') !== remote.app.getVersion()))
}
// config.set('version', remote.app.getVersion())
// var hasEmptyConfig = (config.size == 0) || (config.size == 1 && config.has('version'))

// read config (will initialise inputShowMap, inputShowChat and inputRunServer
readConfig()

window.onbeforeunload = (e) => {
  // always write current version to config before window closes
  config.set('version', remote.app.getVersion())
}

var contentActivators = document.querySelectorAll('header button[data-activates-content]');

contentActivators.forEach( (b) => {
	b.addEventListener('click', () => {
		activateWindowContent(b.dataset.activatesContent)
	})
})

function activateWindowContent(id) {
    Array.from(document.querySelectorAll('.window-content')).forEach( (wc) => {
      wc.className = 'window-content'
    })

  document.querySelector(id).className = 'window-content active'

}

buttonSave.addEventListener('click', () => {
  saveConfig()
})

buttonDelete.addEventListener('click', () => {
  deleteConfig()
})

buttonResetWindows.addEventListener('click', () => {
  console.log('buttonResetWindows')
  resetWindows()
})

buttonToggleWindows.addEventListener('click', () => {
  console.log('buttonToggleWindows')
  ipcRenderer.send('toggle-window-state')
})

buttonToggleZwiftGPS.addEventListener('click', () => {
  console.log('buttonToggleZwiftGPS')
  ipcRenderer.send('toggle-zwiftgps-state')
})

buttonWorld1.addEventListener('click', () => {
  ipcRenderer.send('set-world', 1)
})

document.querySelector('#button-sign-in-myzwiftcom').addEventListener('click', () => {
  console.log('button-sign-in-myzwiftcom click')
  ipcRenderer.send('show-myzwiftcom', true, false)
})

document.querySelector('#button-clear-cache-myzwiftcom').addEventListener('click', () => {
  console.log('button-clear-cache-myzwiftcom click')
  ipcRenderer.send('clear-window-cache','myzwiftcom')
})

document.querySelector('#button-clear-cache-zwiftgps').addEventListener('click', () => {
  console.log('button-clear-cache-zwiftgps click')
  inputShowZwiftGPS.checked = false
  ipcRenderer.send('clear-window-cache','zwiftgps')
  if (!inputAutoLoginZwiftGPS.checked) {
    ipcRenderer.send('set-global', 'riderid', undefined)
  }
})


buttonWorld2.addEventListener('click', () => {
  ipcRenderer.send('set-world', 2)
})

buttonWorld3.addEventListener('click', () => {
  ipcRenderer.send('set-world', 3)
})

buttonWorld4.addEventListener('click', () => {
  ipcRenderer.send('set-world', 4)
})

buttonWorld5.addEventListener('click', () => {
  ipcRenderer.send('set-world', 5)
})

buttonWorld6.addEventListener('click', () => {
  ipcRenderer.send('set-world', 6)
})

buttonWorld7.addEventListener('click', () => {
  ipcRenderer.send('set-world', 7)
})

buttonWorld8.addEventListener('click', () => {
  ipcRenderer.send('set-world', 8)
})


buttonWorld9.addEventListener('click', () => {
  ipcRenderer.send('set-world', 9)
})

buttonWorld10.addEventListener('click', () => {
  ipcRenderer.send('set-world', 10)
})

buttonWorld11.addEventListener('click', () => {
  ipcRenderer.send('set-world', 11)
})

ipcRenderer.on('got-profile', (event, profile) => {
  console.log('got-profile', profile)
  inputRiderId.value = profile.id
  inputWeight.value = profile.weight / 1000
  buttonSaveWeightKg.dispatchEvent(new Event('click', { bubbles: true }))

  if (inputAutoCloseMyZwiftCom.checked) {
    ipcRenderer.send('show-myzwiftcom', false)
  }

  if (inputAutoLoginZwiftGPS.checked) {
    ipcRenderer.send('set-global', 'riderid', profile.id )
  } else {
      ipcRenderer.send('set-global', 'riderid', undefined)
  }

})

inputAutoLoginZwiftGPS.addEventListener('change', () => {
  if (!inputAutoLoginZwiftGPS.checked) {
    ipcRenderer.send('set-global', 'riderid', undefined)
  } else {
    if (inputRiderId.value > 0) {
      ipcRenderer.send('set-global', 'riderid', inputRiderId.value)
    }
  }
})

inputRiderId.addEventListener('change', () => {
  if (inputAutoLoginZwiftGPS.checked) {
    ipcRenderer.send('set-global', 'riderid', inputRiderId.value)
  }
})

buttonSaveSignIn.addEventListener('click', () => {
  ipcRenderer.send('begin-save-sign-in')
})




var signedIn = false;
// buttonSignIn.addEventListener('click', () => {
//   if (signedIn) {
//     signedIn = false
//     buttonSignIn.innerHTML = 'Sign in'
//     captionSignIn.innerHTML = 'Sign in to Zwift'
//     ipcRenderer.send('sign-out')

//     updateGhostsButton()
//   } else {
//     ipcRenderer.send('begin-sign-in')
//   }
// })

// ipcRenderer.on('signed-in', (event, name) => {
//   signedIn = true
//   buttonSignIn.innerHTML = 'Sign out'
//   captionSignIn.innerHTML = `Signed in as ${name}`

//   updateGhostsButton()
// })

// function updateGhostsButton() {
//     buttonEditGhosts.disabled = !signedIn || !inputRunServer.checked
// }

// buttonEditGhosts.addEventListener('click', () => {
//   ipcRenderer.send('edit-ghosts')
// })

inputShowMap.addEventListener('change', () => {
    showMap = inputShowMap.checked
    console.log('caught change event')
    // do some stuff based on value
    asOverlay = inputAsOverlay.checked
    ipcRenderer.send('show-map', showMap, asOverlay)
})

inputShowZwiftGPS.addEventListener('change', () => {
    showZwiftGPS = inputShowZwiftGPS.checked
    console.log('caught change event')
    // do some stuff based on value
    asOverlay = inputAsOverlay.checked
    ipcRenderer.send('show-zwiftgps', showZwiftGPS, asOverlay)
})

document.querySelector('#button-connect-to-zwiftgps').addEventListener('click', () => {
    inputShowZwiftGPS.checked = true
    inputShowZwiftGPS.dispatchEvent(new Event('change', { bubbles: true }))
    // ipcRenderer.send('show-zwiftgps', showZwiftGPS, asOverlay)
})

inputShowChat.addEventListener('change', () => {
    showChat = inputShowChat.checked
    // do some stuff based on value
    asOverlay = inputAsOverlay.checked
    ipcRenderer.send('show-chat', showChat, asOverlay)
})

inputShowStat.addEventListener('change', () => {
    showStat = inputShowStat.checked
    // do some stuff based on value
    asOverlay = inputAsOverlay.checked
    ipcRenderer.send('show-stat', showStat, asOverlay)
})

buttonNextDisplay.addEventListener('click', () => {
  let mainWindow = remote.getCurrentWindow()
  // console.log(mainWindow)
  let mainDisplay = remote.screen.getDisplayNearestPoint({x: mainWindow.getBounds().x, y: mainWindow.getBounds().y})
  // console.log(mainDisplay)
  // alert("Currently: " + mainDisplay.id + " " + mainDisplay.bounds.x + " "  + mainDisplay.bounds.y)
  let allDisplays = remote.screen.getAllDisplays()
  // alert(allDisplays.length)
  allDisplays.push(allDisplays[0])
  // alert(allDisplays.length)
  nextDisplayIndex = 1 + allDisplays.findIndex((element) => {return (element.id == mainDisplay.id)})
  // alert(nextDisplayIndex + "  " + allDisplays[nextDisplayIndex].id)
  var x = allDisplays[nextDisplayIndex].bounds.x + (allDisplays[nextDisplayIndex].bounds.width - mainWindow.getBounds().width) / 2
  var y = allDisplays[nextDisplayIndex].bounds.y + (allDisplays[nextDisplayIndex].bounds.height - mainWindow.getBounds().height) / 2
  
  x = Number(x.toFixed())
  y = Number(y.toFixed())
  
  // alert(mainWindow.getBounds().x +" ; " + mainWindow.getBounds().y + " --> " + x  + " ; " + y )
  
  mainWindow.setPosition(x, y)
})

buttonPreviousDisplay.addEventListener('click', () => {
  let mainWindow = remote.getCurrentWindow()
  let mainDisplay = remote.screen.getDisplayNearestPoint({x: mainWindow.getBounds().x, y: mainWindow.getBounds().y})
  // console.log(mainDisplay)
  let allDisplays = remote.screen.getAllDisplays().reverse()
  allDisplays.push(allDisplays[0])
  nextDisplayIndex = 1 + allDisplays.findIndex((element) => {return (element.id == mainDisplay.id)})
  
  var x = allDisplays[nextDisplayIndex].bounds.x + (allDisplays[nextDisplayIndex].bounds.width - mainWindow.getBounds().width) / 2
  var y = allDisplays[nextDisplayIndex].bounds.y + (allDisplays[nextDisplayIndex].bounds.height - mainWindow.getBounds().height) / 2
  
  x = Number(x.toFixed())
  y = Number(y.toFixed())
  
  // alert(mainWindow.getBounds().x +" ; " + mainWindow.getBounds().y + " --> " + x  + " ; " + y )

  mainWindow.setPosition(x, y)
})

buttonDefaultPositions.addEventListener('click', () => {
  ipcRenderer.send('default-positions')
})

buttonGatherWindows.addEventListener('click', () => {
  ipcRenderer.send('gather-windows')
})
  
function updateServer() {
  runServer = inputRunServer.checked
  serverPort = parseInt(inputServerPort.value)

  // do some stuff based on value
  ipcRenderer.send('run-server', runServer, serverPort)
  // updateGhostsButton()
}

function getHostName() {
  return os.hostname() || 'localhost'
}

function clickedServer() {
  var addr = 'http://' + getHostName() + ':' + parseInt(inputServerPort.value)
  require('opn')(addr)
}

inputRunServer.addEventListener('change', updateServer)
inputServerPort.addEventListener('change', updateServer)
inputServerAddr.addEventListener('click', clickedServer)
inputServerAddr.innerText = 'http://' + getHostName() + ':'

inputShowJustMe.addEventListener('change', () => {
    showJustMe = inputShowJustMe.checked
    // do some stuff based on value
    var track = { 'me': true, 'followees': !showJustMe }
    ipcRenderer.send('change-trackers', track)
})

inputHideRunningOnlyRoads.addEventListener('change', () => {
  hideRunningOnlyRoads = inputHideRunningOnlyRoads.checked
  // do some stuff based on value
  ipcRenderer.send('change-roads', { 'hide-running-only-roads': hideRunningOnlyRoads })
})


inputUseDefaultColor.addEventListener('change', () => {
    useDefaultColor = inputUseDefaultColor.checked
    // do some stuff based on value
    //ipcRenderer.send('xxxxxx', useDefaultColor)
})

inputAsOverlay.addEventListener('change', () => {
    asOverlay = inputAsOverlay.checked
    // do some stuff based on value
    //ipcRenderer.send('xxxxxx', useDefaultColor)
    showZwiftGPS = inputShowZwiftGPS.checked
    showMap = inputShowMap.checked
    showChat = inputShowChat.checked
    showStat = inputShowStat.checked
    ipcRenderer.send('show-stat', showStat, asOverlay)
    ipcRenderer.send('show-chat', showChat, asOverlay)
    ipcRenderer.send('show-map', showMap, asOverlay)
    ipcRenderer.send('show-zwiftgps', showZwiftGPS, asOverlay)
})

inputWindowMode.addEventListener('change', () => {
  //
})

inputDisableHWAcceleration.addEventListener('change', () => {
  //
})

buttonFullscreen.addEventListener('click', () => {
  if (is.windows()) {
    opn(remote.app.getAppPath() + '\\src\\helpers\\pseudofullscreen.exe');
  }
})

document.querySelector('#loglevel').addEventListener('change', () => {
  ipcRenderer.send('set-log-level', document.querySelector('#loglevel').value)
}) 

Array.from(document.querySelectorAll('#background button')).forEach( (b) => {
  b.addEventListener('click', (event) => {
    let opacity = event.currentTarget.getAttribute('value');
    backgroundOpacity(opacity)
  })
})

buttonSaveWeightKg.addEventListener('click', () => {
  // ipcRenderer.send('set-weight', inputWeight.value)
  setWeightUnit('kg', inputWeight.value)
})

buttonSaveWeightLbs.addEventListener('click', () => {
  // ipcRenderer.send('set-weight', inputWeight.value * 0.453592)
  setWeightUnit('lbs', inputWeight.value)
})


// ---

window.addEventListener('load', () => {
    var elems = document.querySelectorAll('[data-help]')
    // var values =
    elems.forEach( (obj) => {
      // alert(obj.innerHTML);
      hpfy.__(obj);
      // return obj.value;
    });


    document.querySelectorAll('[data-platform="' + process.platform + '"]').forEach( (obj) => {
      obj.hidden = false;
    })
    
    
    if (isFirstRun()) {
      document.querySelectorAll('.first-run').forEach( (obj) => {
        if ((obj.innerText = hpfy.text().__(obj.id)) !== '') obj.hidden = false;
      })
      activateWindowContent(document.querySelector('header button[data-activates-content].click-on-first-run').dataset.activatesContent)
    }

    if (isFirstVersionRun() && !isFirstRun()) {
      document.querySelectorAll('.first-version-run').forEach( (obj) => {
        if ((obj.innerText = hpfy.text().__(obj.id + '.' + remote.app.getVersion())) !== '') obj.hidden = false;
      })
    }

    if (!isFirstRun() && !isFirstVersionRun()) {
      // minimize configuration depending on setting (but never when this is a new version / first run)
      if (!remote.getGlobal('errors')) {
        if (minimizeConfiguration) {
          remote.getCurrentWindow().minimize();
        }
      }
    }

    if (remote.getGlobal('errors')) {
      var txt = ''
      remote.getGlobal('errors').forEach( (e) => {
        txt += '<div>' + e + '</div>'
      })
      document.querySelector('#error-message').innerHTML = txt
      document.querySelector('#error-message').hidden = false
    }

})
  



// Function definitions after this point ***************

function saveConfig () {
  
  config.set('enabled.cap', inputEnableCap.checked)

  config.set('show.zwiftgps', inputShowZwiftGPS.checked)
  config.set('show.map', inputShowMap.checked)
  config.set('show.chat', inputShowChat.checked)
  config.set('show.stat', inputShowStat.checked)
  config.set('zwiftgps.reset-zoom', inputResetZoom.checked)
  config.set('hide.running-only-roads', inputHideRunningOnlyRoads.checked)
  config.set('show.justme', inputShowJustMe.checked)
  config.set('server.run', inputRunServer.checked)
  config.set('server.port', inputServerPort.value)

  config.set('myzwiftcom.auto-login', inputAutoRetrieveProfile.checked)
  config.set('myzwiftcom.auto-close', inputAutoCloseMyZwiftCom.checked)
  config.set('zwiftgps.auto-login', inputAutoLoginZwiftGPS.checked)

  weightUnit = document.querySelector('#save-weight-unit button.active').getAttribute('value')
  config.set('rider.save-weight-unit', weightUnit)
  config.set('rider.weight', inputWeight.value)

  config.set('rider.id', inputRiderId.value)

  opacity = document.querySelector('#background button.active').getAttribute('value')
  config.set('background.opacity', opacity)
  // ipcRenderer.send('change-background', '' + opacity/100)

  config.set('color.use-default-color', inputUseDefaultColor.checked)
  config.set('color.map', inputMapColor.value)
  config.set('color.rider', inputRiderColor.value)
  config.set('color.me', inputMeColor.value)
  config.set('minimize.configuration', inputMinimizeConfiguration.checked)
  config.set('mode.as-overlay', inputAsOverlay.checked)
  config.set('mode.window', inputWindowMode.checked)
  config.set('graphics.disable-hw-acceleration', inputDisableHWAcceleration.checked)

  color = config.get('color')
  ipcRenderer.send('change-color', color) 

  config.set('version', remote.app.getVersion())

} // saveConfig

function deleteConfig () {
  config.clear()
  config.set('version', remote.app.getVersion())
} // deleteConfig

function readConfig () {

  enableCap = (config.has('enabled.cap') ? config.get('enabled.cap') : false)
  inputEnableCap.checked = enableCap

  showZwiftGPS = (config.has('show.zwiftgps') ? config.get('show.zwiftgps') : false)
  showMap = (config.has('show.map') ? config.get('show.map') : false)
  showChat = (config.has('show.chat') ? config.get('show.chat') : false)
  // showStat defaults to false
  showStat = (config.has('show.stat') ? config.get('show.stat') : false)
  // hideRunningOnlyRoads defaults to false
  hideRunningOnlyRoads = (config.has('hide.running-only-roads') ? config.get('hide.running-only-roads') : false)
  // showJustMe defaults to false
  showJustMe = (config.has('show.justme') ? config.get('show.justme') : false)

  runServer = (config.has('server.run') ? config.get('server.run') : false)
  serverPort = (config.has('server.port') ? config.get('server.port') : 8080)
  opacity = (config.has('background.opacity') ? config.get('background.opacity') : 50)
  useDefaultColor = (config.has('color.use-default-color') ? config.get('color.use-default-color') : true)
  mapColor = (config.has('color.map') ? config.get('color.map') : '#e35205')
  riderColor = (config.has('color.rider') ? config.get('color.rider') : '#0080ff')
  meColor = (config.has('color.me') ? config.get('color.me') : '#ffffff')
  minimizeConfiguration = (config.has('minimize.configuration') ? config.get('minimize.configuration') : true)
  asOverlay = (config.has('mode.as-overlay') ? config.get('mode.as-overlay') : true)
  windowMode = (config.has('mode.window') ? config.get('mode.window') : false)
  disableHWAcceleration = (config.has('graphics.disable-hw-acceleration') ? config.get('graphics.disable-hw-acceleration') : false)
  
  weightUnit = (config.has('rider.save-weight-unit') ? config.get('rider.save-weight-unit') : 'kg')
  weight = (config.has('rider.weight') ? config.get('rider.weight') : 0) 

  id = (config.has('rider.id') ? config.get('rider.id') : 0) 

  // let remote global values override defaults
  defaultMyZwiftCom = {
    ...{
      'auto-login': false,
      'auto-close': false
    },
    ...remote.getGlobal('myzwiftcom')
  }

  defaultZwiftGPS = {
    ...{
      'auto-login': false, 
      'reset-zoom': true
    },
    ...remote.getGlobal('zwiftgps')
  }
  
  console.log(defaultMyZwiftCom)
  myzwiftcomAutoLogin = (config.has('myzwiftcom.auto-login') ? config.get('myzwiftcom.auto-login') : defaultMyZwiftCom['auto-login'])
  myzwiftcomAutoClose = (config.has('myzwiftcom.auto-close') ? config.get('myzwiftcom.auto-close') : defaultMyZwiftCom['auto-close'])
  
  zwiftgpsResetZoom = (config.has('zwiftgps.reset-zoom') ? config.get('zwiftgps.reset-zoom') : defaultZwiftGPS['reset-zoom'])
  zwiftgpsAutoLogin = (config.has('zwiftgps.auto-login') ? config.get('zwiftgps.auto-login') : defaultZwiftGPS['auto-login'])
  
  if ((id > 0) && zwiftgpsAutoLogin) {
    ipcRenderer.send('set-global', 'riderid', id)
  }
  
  
  inputAutoRetrieveProfile.checked = myzwiftcomAutoLogin
  inputAutoCloseMyZwiftCom.checked = myzwiftcomAutoClose
  inputAutoLoginZwiftGPS.checked = zwiftgpsAutoLogin
  inputResetZoom.checked = zwiftgpsResetZoom

  ipcRenderer.send('set-global', 'zwiftgps',  {'auto-login': zwiftgpsAutoLogin, 'reset-zoom': zwiftgpsResetZoom})

  // my.zwift.com login
  if (myzwiftcomAutoLogin) {
    ipcRenderer.send('set-global', 'myzwiftcom', {'auto-login': myzwiftcomAutoLogin, 'auto-close': myzwiftcomAutoClose})
    ipcRenderer.send('show-myzwiftcom', true, false)
  }

  // if (zwiftgpsAutoLogin) {
  //   ipcRenderer.send('set-global', 'riderid', ?????? )
  // }

  // login
  // signIn = (config.has('login.save') ? config.get('login.save') : false)
  // if (signIn && config.has('login.uername') && config.has('login.password')) {
  //   ipcRenderer.send('begin-silent-sign-in')
  //   ipcRenderer.send('sign-in', config.get('login.uername'), config.get('login.password'))
  // }
  
  // savelogin
  signIn = (config.has('login.save') ? config.get('login.save') : false)
  if (signIn && config.has('login.username') && config.has('login.password')) {
    ipcRenderer.send('save-sign-in', config.get('login.username'), config.get('login.password'))
  }
  
  if (remote.getGlobal('mode.window')) {
    // override configuration if global mode.as-window is true (e.g. if started with flag --window)
    windowMode = true
  }
  
  if (windowMode) {
    // forced windowMode overrides asOverlay
    asOverlay = false
  }
  
  inputMinimizeConfiguration.checked = minimizeConfiguration
  inputAsOverlay.checked = asOverlay
  inputWindowMode.checked = windowMode
  inputDisableHWAcceleration.checked = disableHWAcceleration
  
  inputHideRunningOnlyRoads.checked = hideRunningOnlyRoads
  ipcRenderer.send('change-roads', { 'hide-running-only-roads': hideRunningOnlyRoads });

  inputShowJustMe.checked = showJustMe
  track = { 'me': true, 'followees': !showJustMe }
  ipcRenderer.sendSync('change-trackers', track)
  console.log(remote.getGlobal('track'))
  
  backgroundOpacity(opacity)
  

  inputWeight.value = weight
  setWeightUnit(weightUnit, weight)
  // ipcRenderer.send('set-weight', inputWeight.value )

  if (id > 0) inputRiderId.value = id;

  inputUseDefaultColor.checked = useDefaultColor
  inputMapColor.value = mapColor
  inputRiderColor.value = riderColor
  inputMeColor.value = meColor
  
  color = {
    'use-default-color': useDefaultColor ,
    'map': mapColor,
    'rider': riderColor,
    'me': meColor
  }
  ipcRenderer.sendSync('change-color', color) 
  console.log(remote.getGlobal('color'))
  
  inputShowZwiftGPS.checked = showZwiftGPS
  ipcRenderer.send('show-zwiftgps', showZwiftGPS, asOverlay)
  
  inputShowMap.checked = showMap
  ipcRenderer.send('show-map', showMap, asOverlay)
  
  inputShowChat.checked = showChat
  ipcRenderer.send('show-chat', showChat, asOverlay)
  
  inputShowStat.checked = showStat
  ipcRenderer.send('show-stat', showStat, asOverlay)
  
  inputRunServer.checked = runServer
  inputServerPort.value = serverPort
  ipcRenderer.send('run-server', runServer, serverPort)
  
  
  document.querySelector('#output-config-file').innerHTML = '<a href="' + config.path + '" target="_blank">Configuration file</a>';
  document.querySelector('#open-config-file').addEventListener('click', () => {
    if (is.windows()) {
      opn(config.path, {app: ['notepad']});
    } else if (is.macOS()) {
      opn(config.path, {app: ['textedit']});
    }
  })
  
  document.querySelector('#output-log-file').innerHTML = '<a href="' + remote.getGlobal('log').file + '" target="_blank">Log file</a>';
  document.querySelector('#open-log-file').addEventListener('click', () => {
    if (is.windows()) {
      opn(remote.getGlobal('log').file, {app: ['notepad']});
    } else if (is.macOS()) {
      opn(remote.getGlobal('log').file, {app: ['textedit']});
    }
  })

  
  document.querySelector('#about-enable-cap').innerHTML = ' &nbsp;<a href="how-to-activate-cap.html" target="_blank">Read first...</a>';

  if (!enabled.stat) {
    document.querySelector('#how-to-activate-stat').innerHTML = ' &nbsp;<a href="how-to-activate-cap.html" target="_blank">How to activate</a>';
  }
  if (!enabled.chat ) {
    document.querySelector('#how-to-activate-chat').innerHTML = ' &nbsp;<a href="how-to-activate-cap.html" target="_blank">How to activate</a>';
  }


} // readConfig


function backgroundOpacity(opacity) {
  Array.from(document.querySelectorAll('#background button')).forEach( (b) => {
    b.className = 'btn btn-default'
  })
  
  document.querySelector('button#background-' + opacity).className = 'btn btn-default active'
  
  ipcRenderer.send('change-background', '' + opacity/100) 
  
}


function setWeightUnit(unit = 'kg', weight = 0) {
  Array.from(document.querySelectorAll('#save-weight-unit button')).forEach( (b) => {
    b.className = 'btn btn-default'
  })
  
  document.querySelector('button#save-weight-' + unit).className = 'btn btn-default active'
  
  if (weight > 0) {
    ipcRenderer.send('set-weight', weight * (unit == 'kg' ? 1 : 0.453592)) 
  }
  
}



function resetWindows () {
  ipcRenderer.send('clear-window-state')
} // resetWindows



// Additional event handlers after this point *************

// ipc messages to handle:
ipcRenderer.on('config-do-this-do-that', (event, message) => {
  // console.log(arg) // prints message
})

