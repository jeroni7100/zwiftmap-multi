# Change Log


### 2.2.2

* Updating to latest electron@13 to fix certificate error (caused problem in ZwiftGPS display)

### 2.2.1

* Fix regex for position because of change in log file (FPS with decimal digits)


### 2.2.0

* Updated to electron as well as other dev dependencies to @latest. Because of this:
* Changes to preload script (use contextBridge)
* Use contextIsolation: false as well as remoteModuleEnabled: true for all windows (quick and dirty fix)
* Disabled profile retrieval from my.zwift.com


### 2.1.8

#### Fixes

* Fix missing ride metrics by updating the network capture module

### 2.1.7

#### Fixes

* Fix support for Crit City in the ZwiftGPS window (showed New York instead of Crit City)

### 2.1.6

#### Changes/features

* Support for France and Paris added to the classic map

#### Fixes

* Fix: Network capture (and thus ride data and chat) not working in the macOS version